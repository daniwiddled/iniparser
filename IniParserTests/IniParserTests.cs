﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IniFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IniFile.Tests
{
    [TestClass()]
    public class IniFileTests
    {
        [TestMethod()]
        public void GetString_FileContainsStrings_StringsReaded()
        {
            File.Delete("test2.ini");

            File.WriteAllLines("test2.ini", new String[] {
                "[GENERAL]", "sKey1='Test'", "sKey2='Super'", "[WINDOW]", "sKey1='Hallo'", ";sKey8='h'"
            });

            IniFile file = new IniFile("test2.ini");

            Assert.AreEqual("Test", file.GetString("GENERAL", "Key1"));
            Assert.AreEqual("Super", file.GetString("GENERAL", "Key2"));
            Assert.AreEqual("Super", file.GetString("WINDOW", "Key8", "Super"));
            Assert.AreEqual("ERROR", file.GetString("GENERAL", "Key3", "ERROR"));
            Assert.AreEqual("ERROR", file.GetString("TEST", "Key1", "ERROR"));
            Assert.AreEqual("Hallo", file.GetString("WINDOW", "Key1"));

            try
            {
                file.GetString("GENERAL", "Key4");
                Assert.Fail();
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod()]
        public void GetBoolean_FileContainsBooleans_BooleansReaded()
        {
            File.Delete("test3.ini");

            File.WriteAllLines("test3.ini", new String[] {
                "[GENERAL]", "bKey1=false", "bKey2=true"
            });

            IniFile file = new IniFile("test3.ini");

            Assert.AreEqual(false, file.GetBoolean("GENERAL", "Key1"));
            Assert.AreEqual(true, file.GetBoolean("GENERAL", "Key2"));
            Assert.AreEqual(false, file.GetBoolean("GENERAL", "Key3", false));
            Assert.AreEqual(true, file.GetBoolean("TEST", "Key1", true));

            try
            {
                file.GetBoolean("GENERAL", "Key4");
                Assert.Fail();
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod()]
        public void GetInteger_FileContainsInteger_IntegerReaded()
        {
            File.Delete("test4.ini");

            File.WriteAllLines("test4.ini", new String[] {
                "[GENERAL]", "iKey1=5", "iKey2=34"
            });

            IniFile file = new IniFile("test4.ini");

            Assert.AreEqual(5, file.GetInteger("GENERAL", "Key1"));
            Assert.AreEqual(34, file.GetInteger("GENERAL", "Key2"));
            Assert.AreEqual(888, file.GetInteger("GENERAL", "Key3", 888));
            Assert.AreEqual(456, file.GetInteger("TEST", "Key1", 456));

            try
            {
                file.GetInteger("GENERAL", "Key4");
                Assert.Fail();
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod()]
        public void GetDouble_FileContainsDouble_DoubleReaded()
        {
            File.Delete("test5.ini");

            File.WriteAllLines("test5.ini", new String[] {
                "[GENERAL]", "dKey1=8.234", "dKey2=555.12", "dKey4=0.3"
            });

            IniFile file = new IniFile("test5.ini");

            Assert.AreEqual(8.234, file.GetDouble("GENERAL", "Key1"));
            Assert.AreEqual(555.12, file.GetDouble("GENERAL", "Key2"));
            Assert.AreEqual(50, file.GetDouble("GENERAL", "Key3", 50));
            Assert.AreEqual(890, file.GetDouble("TEST", "Key1", 890));
            Assert.AreEqual(0.3, file.GetDouble("GENERAL", "Key4"));

            try
            {
                file.GetDouble("GENERAL", "Key5");
                Assert.Fail();
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod()]
        public void SetString_KeyAlreadyExists_ValueChanged()
        {
            File.Delete("test6.ini");

            File.WriteAllLines("test6.ini", new String[] {
                "[GENERAL]", "sKey1='Hallo'", "dKey2=555,12"
            });

            IniFile file = new IniFile("test6.ini");

            file.SetString("GENERAL", "Key1", "Ciao");

            Assert.AreEqual("Ciao", file.GetString("GENERAL", "Key1"));
        }

        [TestMethod()]
        public void SetString_KeyDoesntExist_ValueChanged()
        {
            File.Delete("test7.ini");

            File.WriteAllLines("test7.ini", new String[] {
                "[GENERAL]", "sKey1='Hallo'", "dKey2=555,12"
            });

            IniFile file = new IniFile("test7.ini");

            file.SetString("GENERAL", "Key3", "Ciao");

            Assert.AreEqual("Ciao", file.GetString("GENERAL", "Key3"));
        }

        [TestMethod()]
        public void SetString_SectionDoesntExist_ValueChanged()
        {
            File.Delete("test8.ini");

            File.WriteAllLines("test8.ini", new String[] {
                "[GENERAL]", "sKey1='Hallo'", "dKey2=555,12"
            });

            IniFile file = new IniFile("test8.ini");

            file.SetString("WARNING", "Key3", "Ciao");

            Assert.AreEqual("Ciao", file.GetString("WARNING", "Key3"));
        }

        [TestMethod()]
        public void IniFile_FileDoesntExist_FileCreated()
        {
            File.Delete("test.ini");

            IniFile file = new IniFile("test.ini");

            Assert.AreEqual(true, File.Exists("test.ini"));
        }
    }
}