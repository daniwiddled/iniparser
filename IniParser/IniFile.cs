﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace IniFile
{
    public class IniFile
    {
        string filePath;
        Logger logger;
        LogStage keyNotFoundLogStage;

        public IniFile(String filePath, Logger logger = null, LogStage keyNotFoundLogStage = LogStage.DEBUG)
        {
            this.filePath = filePath;

            if (File.Exists(this.filePath) == false)
            {
                File.Create(this.filePath);
            }

            this.logger = logger;
            this.keyNotFoundLogStage = keyNotFoundLogStage;
        }

        private String GetValue(String section, String key)
        {
            int lineNumber = GetLineNumberOfKey(section, key);

            if (lineNumber == 0)
            {
                if(logger != null)
                {
                    logger.Log("IniFile.KeyNotFoundException: " + section + ":" + key + " not found", keyNotFoundLogStage);
                }

                throw new KeyNotFoundException(section + ":" + key);
            }
            else
            {
                return File.ReadAllLines(this.filePath)[lineNumber - 1].Substring(key.Length + 1);
            }

        }

        public String GetString(String section, String key)
        {
            String value = GetValue(section, "s" + key);
            return value.Substring(1, value.Length - 2);
        }

        public String GetString(String section, String key, String defaultValue)
        {
            try
            {
                String value = GetValue(section, "s" + key);
                return value.Substring(1, value.Length - 2);
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }

        public int GetInteger(String section, String key)
        {
            return Convert.ToInt32(GetValue(section, "i" + key));
        }

        public int GetInteger(String section, String key, int defaultValue)
        {
            try
            {
                return Convert.ToInt32(GetValue(section, "i" + key));
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }

        public double GetDouble(String section, String key)
        {
            return Convert.ToDouble(GetValue(section, "d" + key).Replace(".", ","));
        }

        public double GetDouble(String section, String key, double defaultValue)
        {
            try
            {
                return Convert.ToDouble(GetValue(section, "d" + key).Replace(".", ","));
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }

        public bool GetBoolean(String section, String key)
        {
            return Convert.ToBoolean(GetValue(section, "b" + key));
        }

        public bool GetBoolean(String section, String key, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(GetValue(section, "b" + key));
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }

        public void SetString(String section, String key, String value)
        {
            SetValue(section, "s" + key, "'" + value + "'");
        }

        public void SetInteger(String section, String key, int value)
        {
            SetValue(section, key, "i" + value.ToString());
        }
        public void SetDouble(String section, String key, double value)
        {
            SetValue(section, key, "d" + value.ToString());
        }
        public void SetBoolean(String section, String key, bool value)
        {
            SetValue(section, key, "b" + value.ToString());
        }

        private void SetValue(String section, String key, String value)
        {
            int lineNumber = GetLineNumberOfKey(section, key);
            List<String> lines = new List<string>();
            lines.AddRange(File.ReadAllLines(this.filePath));

            if (lineNumber == 0)
            {

                if (lines.Contains("[" + section + "]"))
                {
                    lines.Insert(lines.IndexOf("[" + section + "]") + 1, key + "=" + value);
                }
                else
                {
                    lines.Add("[" + section + "]");
                    lines.Add(key + "=" + value);
                }
            }
            else
            {
                lines[lineNumber - 1] = key + "=" + value;
            }

            File.WriteAllLines(this.filePath, lines);
        }

        private int GetLineNumberOfKey(String section, String key)
        {
            bool rightSection = false;
            int lineNumber = 1;

            FileStream fs = null;
            try
            {
                fs = new FileStream(this.filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader sr = new StreamReader(fs))
                {
                    fs = null;

                    while (!sr.EndOfStream)
                    {
                        String line = sr.ReadLine();

                        if (line.StartsWith(";"))
                        {
                            break;
                        }

                        if (line.StartsWith("[") && line.EndsWith("]"))
                        {
                            if (line.Substring(1, line.Length - 2) == section)
                            {
                                rightSection = true;
                            }
                            else
                            {
                                rightSection = false;
                            }
                        }

                        if (rightSection)
                        {
                            if (line.StartsWith(key + "="))
                            {
                                return lineNumber;
                            }
                        }

                        lineNumber++;
                    }
                }
            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }

            return 0;
        }
    }
}
